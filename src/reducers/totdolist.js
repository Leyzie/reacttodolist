// import * as types from '../actions/const'

const initialState = {
    todolist: [
      {
        id: 1,
        name: 'Theodore Roosevelt',
        star: false
      },
      {
        id: 2,
        name: 'Abraham Lincoln',
        star: true
      },
      {
        id: 3,
        name: 'George Washington',
        star: false
      }
    ]
};

  
export default function todo(state = initialState, action) {
  console.log(state);
  
  switch( action.type )
  {
    case 'ADD_TODO': {
      const idnew = state.todolist.length + 1
      const item = {
          id: idnew,
          name: action.payload,
          star: false
        }
        return {...state, todolist: [...state.todolist, item]}
    }
    case 'DELL_ITEM': {
      const todolist = state.todolist.filter(({ id }) => id !== action.payload);
      return { ...state, todolist };
    }
    case 'ADD_STAR': {
      const todolist = state.todolist.map(item => {
        if(item.id === action.payload) {
          const newItem = { ...item}
          newItem.star = !newItem.star
          return newItem
        }
        return item
      })
      return { ...state, todolist}
    }
    default:
      return state;
  }
}