import { combineReducers } from "redux";
import todo from './totdolist'

const rootReducer = combineReducers({
    todo
});

export default rootReducer