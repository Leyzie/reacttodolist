import React from 'react';
import './App.css';
import ToDoList from './components/ToDoList';

function App() {
  return (
    <div className="container mt-3">
      <div className="row align-items-center">
        <div className="col-12 mb-5">
          <h1 className="text-center">
           Title
          </h1>
        </div>
        <div className="col-6">
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium, tenetur esse. Excepturi vel enim vitae molestias laudantium animi, dicta quod?</p>
        </div>
        <div className="col-6">
          <div className="row justify-content-center">
            <ToDoList/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
