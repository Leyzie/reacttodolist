import * as types from './const';

export function addTodo(text) {
  return {
    type: types.ADD_TODO,
    payload: text
  };
}

export function startTodo(id) {
  return {
    type: types.ADD_STAR,
    payload: id
  };
}
export function delItem(id) {
  return {
    type: types.DELL_ITEM,
    payload: id
  };
}
