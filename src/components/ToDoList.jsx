import React, { Component } from 'react'
import { connect } from 'react-redux'

// import { Dispatch } from 'redux';
import Item from '../container/item'
import { addTodo, startTodo, delItem } from '../actions/actions'
class ToDoList extends Component {

    constructor(props){
        super(props)
        this.state = {
            text: '',
            errors: '',
            columnShow: true
        }
        this.inputChange = this.inputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.showBtn = this.showBtn.bind(this)
    }

    inputChange(e) {
        // console.log(e.currentTarget.value);
        this.setState({text: e.currentTarget.value})
                
    }
    handleSubmit(e) {
        e.preventDefault()
        if(this.state.text === '' || this.state.text.length < 0){
            return this.setState({
                errors: 'Missing value'
            })
        }
        this.props.addTodo(this.state.text)
        this.setState({
            text: '',
            errors: '',
        })
        
    }
    showBtn(e) {
        e.preventDefault()
        this.setState({columnShow: !this.state.columnShow})
    }

    render() {

        const arrList = this.props.todolist

        if(arrList === undefined || arrList.length <= 0){
            return (
                <>
                    <p>Пусто</p>
                    <form onSubmit={this.handleSubmit} className="col-12">
                        <div className="form-group row">
                            <input type="text" value={this.state.text} className={`col-9 form-control ${this.state.errors !== '' && 'form-control-error'}`}  placeholder="Add Todo item" onChange={this.inputChange}/>
                            <button className="col-3 btn btn-primary">Add</button>
                        </div>
                        {this.state.errors !== '' &&
                            <div className="alert alert-danger" role="alert">
                                {this.state.errors}
                            </div>
                        }
                    </form>
                </>
            )
        }
        return (
            <>
                <button className="btn btn-warning col-5 mb-3" onClick={this.showBtn}>{this.state.columnShow? 'Скрыть ID в таблице' : 'Показать ID в таблице'}</button>
                <table className="col-12 table table-bordered">
                    <thead className="thead-dark">
                        <tr>
                           {this.state.columnShow&& <th>id</th>}
                            <th>Text</th>
                            <th>Control</th>
                        </tr>
                    </thead>
                    <tbody>
                        {arrList.map((items) => (
                            <Item
                                key={items.id}
                                id={items.id}
                                name={items.name}
                                star={items.star}
                                funStar={() => this.props.startTodo(items.id)}
                                funDel={() => this.props.delItem(items.id)}
                                showColumn={this.state.columnShow}
                            />
                        ))}
                    </tbody>
                </table>
                <form onSubmit={this.handleSubmit} className="col-12">
                    <div className="form-group row">
                        <input type="text" value={this.state.text} className={`col-9 form-control ${this.state.errors !== '' && 'form-control-error'}`}  placeholder="Add Todo item" onChange={this.inputChange}/>
                        <button className="col-3 btn btn-primary">Add</button>
                    </div>
                    {this.state.errors !== '' &&
                        <div className="alert alert-danger" role="alert">
                            {this.state.errors}
                        </div>
                    }
                </form>
            </>
        )
    }
}

const mapStateToProps = state => {
    console.log(state);
    
    return {
        todolist: state.todo.todolist
    }
    
}

const mapDispatchToProps = dispatch => {
    return {
        addTodo: text => dispatch(addTodo(text)),
        startTodo: id => dispatch(startTodo(id)),
        delItem: id => dispatch(delItem(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ToDoList)
