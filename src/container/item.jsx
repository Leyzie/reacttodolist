import React from 'react';
import PropTypes from 'prop-types';
import starimg from '../assets/img/star.png'

const Item = props => {
    const {id, name, star, funDel, funStar, showColumn} = props
    return (
        <tr>
            {showColumn&&<td>{id}</td>}
            <td>{name}</td>
            <td>
                <button className={`btn btn-success star-${star}`}
                onClick={funStar}> 
                    <img src={starimg} alt="satar"/>
                </button>
                <button className="btn btn-danger" onClick={funDel}> 
                    Dell
                </button>
            </td>
        </tr>
    );
};

Item.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    star: PropTypes.bool.isRequired,
    funStar: PropTypes.func.isRequired,
    funDel: PropTypes.func.isRequired,
    showColumn: PropTypes.bool.isRequired
};

export default Item;